package com.mahasiswa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MahasiswaJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MahasiswaJpaApplication.class, args);
	}

}
