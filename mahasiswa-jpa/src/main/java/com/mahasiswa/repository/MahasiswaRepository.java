package com.mahasiswa.repository;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.mahasiswa.model.Mahasiswa;

public interface MahasiswaRepository extends JpaRepository<Mahasiswa, Integer> {
}