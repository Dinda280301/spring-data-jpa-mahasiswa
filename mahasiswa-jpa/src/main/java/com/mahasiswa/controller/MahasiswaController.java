package com.mahasiswa.controller;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mahasiswa.model.Mahasiswa;
import com.mahasiswa.service.MahasiswaService;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/mhs")
public class MahasiswaController {

	@Autowired
	MahasiswaService mhsService;

	@RequestMapping("/data")
	public ResponseEntity<List<Mahasiswa>> getAllMahasiswa() {
		try {
			System.out.println("getAllMhs");
			return new ResponseEntity<>(mhsService.getAllMahasiswa(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/data/{id}")
	public ResponseEntity<Mahasiswa> getMahasiswaById(@PathVariable("id") int id) {
		if (mhsService.getMahasiswa(id)!=null) {
			return new ResponseEntity<>(mhsService.getMahasiswa(id), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/data")
	public ResponseEntity<Mahasiswa> createTutorial(@RequestBody Mahasiswa mahasiswa) {
		try {
			mhsService.insert(mahasiswa);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/data/{id}")
	public ResponseEntity<Mahasiswa> updateMahasiswa(@PathVariable("id") int id, @RequestBody Mahasiswa mahasiswa) {
		if (mhsService.getMahasiswa(id)!=null) {
			mhsService.update(mahasiswa, id);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/data/{id}")
	public ResponseEntity<HttpStatus> deleteTutorial(@PathVariable("id") int id) {
		try {
			mhsService.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/mhs")
	public ResponseEntity<HttpStatus> deleteAllMahasiswa() {
		try {
			mhsService.delete();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
